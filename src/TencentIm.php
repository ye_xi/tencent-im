<?php


namespace Always\TencentIm;

use Pimple\Container;

/**
 * Class TencentIm
 * @author always <302611431@qq.com>
 *
 * @property \Always\TencentIm\service\Account $account
 * @property \Always\TencentIm\service\Message $message
 * @property \Always\TencentIm\service\Sns $sns
 * @property \Always\TencentIm\service\Contact $contact
 */
class TencentIm extends Container
{
    /**
     * Service Providers.
     *
     * @var array
     */
    protected $providers = [
        ServiceProviders\AccountServiceProvider::class,
        ServiceProviders\MessageServiceProvider::class,
        ServiceProviders\SnsServiceProvider::class,
        ServiceProviders\ContactServiceProvider::class,
    ];

    /**
     * TencentIm constructor.
     */
    public function __construct($config)
    {
        parent::__construct();

        $this['config'] = $config;
        $this->registerProviders();
    }

    /**
     * @param array $providers
     */
    public function registerProviders()
    {
        foreach ($this->providers as $provider) {
            parent::register(new $provider());
        }
    }

    /**
     * Magic get access.
     *
     * @param string $id
     *
     * @return mixed
     */
    public function __get($id)
    {
        return $this->offsetGet($id);
    }

    /**
     * Magic set access.
     *
     * @param string $id
     * @param mixed  $value
     */
    public function __set($id, $value)
    {
        $this->offsetSet($id, $value);
    }
}