<?php

namespace Always\TencentIm\service;

use Always\TencentIm\Client;

class Account extends Client
{
    /**
     * 导入单个用户进入IM系统
     * @param int|string $userId 用户id 必须是字符串 INT会报错
     * @param string $nickname 用户昵称
     * @param string $avatar 用户头像 URL
     * @return mixed
     */
    public function accountImport($userId, $nickname, $avatar)
    {
        $param = [
            'Identifier' => (string)$userId,
            'Nick'       => $nickname,
            'FaceUrl'    => $avatar
        ];
        $url   = "im_open_login_svc/account_import";
        return $this->sendPost($url, $param);
    }

    /**
     * 查询自有帐号是否已导入
     * @param array $ids 检查的好友id数组 [['UserID' => '1']]
     * @return mixed    
     */
    public function accountCheck(array $ids)
    {
        $param = [
            'CheckItem' => $ids
        ];
        $url   = "im_open_login_svc/account_check";
        return $this->sendPost($url, $param);
    }

    /**
     * 导入多个帐号
     * @param array $accounts 用户名，单个用户名长度不超过32字节，单次最多导入100个用户名 ["test1","test2","test3","test4","test5"]
     * @return mixed
     */
    public function multiAccountImport(array $accounts)
    {
        $param = [
            'Accounts' => $accounts
        ];
        $url   = "im_open_login_svc/multiaccount_import";
        return $this->sendPost($url, $param);
    }

    /**
     * 删除帐号
     * @param array $accounts 帐号对象数组，单次请求最多支持100个帐号 [['UserID' => '1']]
     * @return mixed
     */
    public function accountDelete(array $accounts)
    {
        $param = [
            'DeleteItem' => $accounts
        ];
        $url   = "im_open_login_svc/account_delete";
        return $this->sendPost($url, $param);
    }

    /**
     * 用户帐号的登录状态（例如 UserSig）失效
     * @param string|int $userId  用户名
     * @return mixed
     */
    public function kick($userId)
    {
        $param = [
            'UserID' => (string)$userId
        ];
        $url   = "im_open_login_svc/kick";
        return $this->sendPost($url, $param);
    }

    /**
     * 查询帐号在线状态
     * @param array $accounts 需要查询用户名 ["id1", "id2", "id3", "id4"]
     * @param boolean $isNeedDetail 是否返回详细的登录平台信息
     * @return mixed
     */
    public function queryOnlineStatus(array $accounts, $isNeedDetail = false)
    {
        $param = [
            'To_Account' => $accounts
        ];
        if ($isNeedDetail) {
            $param['IsNeedDetail'] = 1;
        }
        $url   = "openim/query_online_status";
        return $this->sendPost($url, $param);
    }
    
}