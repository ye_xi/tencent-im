<?php


namespace Always\TencentIm\ServiceProviders;

use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Always\TencentIm\service\Sns;

class SnsServiceProvider implements ServiceProviderInterface
{
    public function register(Container $pimple)
    {
        !isset($pimple['sns']) && $pimple['sns'] = function ($pimple) {
            return new Sns($pimple['config']);
        };
    }
}